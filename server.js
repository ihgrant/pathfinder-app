"use strict";
const path = require("path");
const express = require("express");
const Sentry = require("@sentry/node");

const initializeApi = require("./api");
const initializeDatabaseClient = require("./sqlite-client");

const { PORT } = process.env;
const port = PORT || 3001;
const app = express();

Sentry.init({
  dsn: "https://db4419113cb9fef45c45423dac339b2c@o4508541808279552.ingest.us.sentry.io/4508541808541696",
});

function main() {
    initializeDatabaseClient().then((databaseClient) => {
        const api = initializeApi(databaseClient);
        app.use("/api", api)
            .use("/", express.static(path.join(__dirname, "dist")))
            .listen(port, function () {
                console.info("server listening on port " + port);
            });
    });
}

main();
