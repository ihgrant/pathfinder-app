"use strict";
const sqlite3 = require("sqlite3");
const { open } = require("sqlite");
const DB_PATH = "data/pathfinder.sqlite";

function initialize() {
    return open({ driver: sqlite3.Database, filename: DB_PATH }).then((db) => {
        function getSpells() {
            return db.all("SELECT * FROM spells_import");
        }

        function getClasses() {
            return db.all("SELECT * FROM classes");
        }

        function getMagicSchools() {
            return db.all("SELECT * FROM magic_schools");
        }

        function getFeats() {
            return db.all("SELECT * FROM feats_import");
        }

        return {
            getClasses,
            getFeats,
            getMagicSchools,
            getSpells,
        };
    });
}

module.exports = initialize;
