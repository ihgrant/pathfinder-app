# pathfinder-app

a minimal web app that allows for quick searching and filtering of spells from the pen & paper RPG Pathfinder. The data comes from a collection of [CSVs][] that I imported to a sqlite database to make more complex queries easier.

[csvs]: http://www.pathfindercommunity.net/home/databases

## demo

https://pathfinder-app.fly.dev/

## installation

This project requires that you have [nodejs](https://nodejs.org/en/) version 18 installed. If you use [nvm](https://github.com/nvm-sh/nvm) installed, running `nvm install` in the repo root will automatically install the correct version and switch to it.

After you download the files or clone the repo, open your terminal emulator of choice, navigate to the repo root, and run `npm install`. When that's finished, enter `npm start` and navigate to the specified url in your browser of choice.

[nodejs]: https://nodejs.org

## to do

-   make it more mobile-friendly
-   support url queries corresponding to filter input
-   filter spells by magic school

---

All RPG content is (c) Paizo Publishing, LLC.
